// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");
const path=require("path")
// Khởi tạo app express
const app = express();

// Khai báo cổng của project
const port = 8000;
// khai báo sử dụng tài nguyên statis (images, js, css,....)
app.use(express.static('views'));

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/Pizza 365.html"));

})
// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})